package com.company;

class Person {

    protected String myName ;   // name of the person
    protected int myAge;        // person�s age
    protected String myGender;  // �M� for male, �F� for female

    public Person(String name, int age, String gender)  {
        myName = name;
        myAge = age ;
        myGender = gender;
    }

    public String getName(){
        return myName;
    }

    public int getAge() {
        return myAge;
    }

    public String getGender() {
        return myGender;
    }

    public void setName(String name) {
        myName=name;
    }

    public void setAge(int age) {
        myAge=age;
    }

    public void setGender(String gender) {
        myGender=gender;
    }
    public String toString()  {
        return myName + ", age: " + myAge + ", gender: " +myGender;
    }
}

class Student extends Person {
    protected String myIdNum;    // Student Id Number
    protected double myGPA;      // grade point average

    public Student(String name, int age, String gender, String idNum, double gpa)  {
        super(name, age, gender); // use the super class� constructor
        myIdNum = idNum;
        myGPA = gpa;
    }

    public String getIdNum() {
        return myIdNum;
    }

    public double getPDA() {
        return myGPA;
    }

    public void setPDA(int pd) {
        myGPA=pd;
    }

    public String toString()  {
        return myIdNum + ", gpa: " + myGPA ;
    }
}

class Teacher extends Person {
    protected String mySubject;
    protected int mySalary;

    public Teacher(String nu, int age, String gen, String subject, int salary) {
        super(nu,age,gen);
        mySubject=subject;
        mySalary=salary;
    }

    public String getSubject(){
        return mySubject;
    }

    public int getSalary(){
        return mySalary;
    }

    public void setSubject(String subject) {
        mySubject=subject;
    }

    public void setSalary(int salary) {
        mySalary=salary;
    }
    public String toString()  {
        return "subject"+mySubject + ", salariu: " + mySalary ;
    }
}

class CollegeStudent extends Student {
    protected String major;
    protected int year;

    public CollegeStudent(String name, int age, String gender, String idNum, double gpa, String maj, int ye) {
        super(name, age, gender, idNum, gpa);
        major = maj;
        year = ye;
    }

    public String getMajor() {
        return major;
    }

    public int getYear() {
        return year;
    }

    public void setMajor(String maj) {
        major=maj;
    }

    public void setYear(int ye) {
        year=ye;
    }

    public String toString()  {
        return "major "+ major + ", year " + year ;
    }
}


public class Main {

    public static void main(String[] args) {
        Person bob = new Person("Coach Bob", 27, "M");
        System.out.println(bob);

        Teacher mrJava = new Teacher("Duke Java", 34, "M", "Computer Science", 50000);
        System.out.println(mrJava);

        CollegeStudent ima = new CollegeStudent("Ima Frosh", 18, "F", "UCB123", 4.0, "English", 1);

        System.out.println(ima);
    }
}
