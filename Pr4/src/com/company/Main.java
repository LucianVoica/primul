package com.company;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

interface getShape {
     String printName();
}

class Shape implements getShape{
    protected String myColor;

    public Shape (String color){
        myColor=color;
    }

    public double getArea() { return 0;}

    public String toString() {
        return "culoare: " + myColor;
    }

    public String printName(){
        return "forma";
    }
}

class Triangle extends Shape{
    protected int myBase,myHeight;

    public Triangle(String color, int base, int height){
        super(color);
        myBase=base;
        myHeight=height;
    }

    public double getArea(){
        return myBase*myHeight/2;
    }

    public String toString() {
        return super.toString()+" baza: "+ myBase + " inaltime: "+myHeight;
    }

    public String printName() {
        return "triunghi";
    }
}

class Rectangle extends Shape {
    protected int myLength,myWidth;

    public Rectangle(String color, int length, int width){
        super(color);
        myLength=length;
        myWidth=width;
    }

    public double getArea(){
        return myLength*myWidth;
    }

    public String toString() {
        return super.toString()+" lungime: "+ myLength + " latime: "+myWidth;
    }

    public String printName() {
        return "dreptunghi";
    }
}

public class Main {

    public static void main(String[] args) {

        Shape s1,s2,s3;
        s1=new Shape("green");
        s2=new Triangle("blue",10,5);
        s3 = new Rectangle("yellow",10,5);
        /*
        System.out.println(s1);
        System.out.println(s2);
        System.out.println("Area is " + s2.getArea());
        System.out.println(s3);
        System.out.println("Area is " + s3.getArea());
        System.out.println(s1.printName());
        System.out.println(s2.printName());
        System.out.println(s3.printName());
        */
        Collection<Shape> coll= new ArrayList<Shape>();
        coll.add(s1);
        coll.add(s2);
        coll.add(s3);
        Iterator<Shape> it=coll.iterator();
        while(it.hasNext()) {
            it.next().printName();
        }
    }
}
