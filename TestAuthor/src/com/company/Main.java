package com.company;

class Author {

    private String name, email, gender;

    public Author(String nu, String em, String ge) {
        name=nu;
        email=em;
        gender=ge;
    }

    public String getName () {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public void setEmail(String em) {
        email=em;
    }

    public String toString() {
        return "autor: " + name + "\ne-mail: " + email + "\ngender: "+gender;
    }
}

class Book {
    private String name;
    private Author author;
    private double price;
    private int qtyInStock;

    public Book(String nu, Author au, double pr, int qt ){
        name=nu;
        author=au;
        price=pr;
        qtyInStock=qt;
    }

    public String getName () {
        return name;
    }

    public Author getAuthor () {
        return author;
    }

    public double getPrice () {
        return price;
    }

    public int getQtyInStock () {
        return qtyInStock;
    }

    public void setPrice(double pr) {
        price=pr;
    }

    public void setQtyInStock(int qt){
        qtyInStock=qt;
    }

    public String toString() {
        return "carte: " + name + "\nautor: " + author + "\npret: "+price+"\ncantitate disponibila"+qtyInStock+"\n";
    }
}

public class Main {

    public static void main(String[] args) {

	Author anAuthor=new Author("voica","voica.lucian","m");
         System.out.println(anAuthor);

    Book aBook=new Book("myBook",anAuthor,12.99,1000);
        System.out.println(aBook);
        System.out.println(aBook.getName());
        aBook.setPrice(42.95);
        System.out.println(aBook.getPrice());
        System.out.println(aBook.getAuthor());
    }
}
